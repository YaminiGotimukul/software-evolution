#ifndef _SIMPLEMATH_H_
#define _SIMPLEMATH_H_

#include <cmath>

class SimpleMath {

	public:
		SimpleMath();
		virtual ~SimpleMath();
		static void func0(int,int);
		static void func1(int,int);
};

#endif