from __future__ import print_function
import sys
import os
import re

from pycparser import c_parser, c_ast, parse_file

oldfile = None
newfile = None
oldChanges = []
newChanges = []
methods = []
CPPPATH = '../pycparser/utils/cpp.exe' if sys.platform == 'win32' else 'cpp'

class FuncDefVisitor(c_ast.NodeVisitor):
    def visit_FuncDef(self, node):
        loc = node.decl.coord.file, node.decl.name, node.decl.coord.line
        global methods
        methods.append(loc)

def get_method_range(filename):
    # Note that cpp is used. Provide a path to your own cpp or
    # make sure one exists in PATH.
    #
    try:
    	ast = parse_file(filename, use_cpp=True, cpp_path=CPPPATH, cpp_args=r'-I../pycparser/utils/fake_libc_include')
    except:
    	return
    
    v = FuncDefVisitor()
    v.visit(ast)

def get_changed_method():
    oldMethods = []
    newMethods = []
    global methods
    global oldChanges
    global newChanges
    for method in methods:
        if method[0] == oldfile :
            oldMethod = method[1], method[2]
            oldMethods.append(oldMethod)
        if method[0] == newfile :
            newMethod = method[1], method[2]
            newMethods.append(newMethod)
    changedOldMethods = []
    for oldChange in oldChanges:
        for index, method in enumerate(oldMethods) :
            if int(index) == int(len(oldMethods) - 1) :
                if int(oldChange[0]) > int(method[1]) :
                    changedOldMethods.append(method)
            else :
                next = oldMethods[index + 1]
                if int(oldChange[0]) >= int(method[1]) and int(oldChange[1]) <= int(next[1]) :
                    changedOldMethods.append(method)
    
    changedNewMethods = []
    for newChange in newChanges:
        for index, method in enumerate(newMethods) :
            if int(index) == int(len(newMethods) - 1) :
                if int(newChange[0]) > int(method[1]) :
                    changedNewMethods.append(method)
            else :
                next = newMethods[index + 1]
                if int(newChange[0]) >= int(method[1]) and int(newChange[1]) <= int(next[1]) :
                    changedNewMethods.append(method)
    
    changedOldSet = set(changedOldMethods)
    changedNewSet = set(changedNewMethods)
    #print(newfile)
    #print(changedNewMethods)
    methods[:] = []
    oldChanges[:] = []
    newChanges[:] = []
    print(methods)
    return changedNewSet

# Diff the new and the old version of the c programs and returns the set of tuples of the method name , line number of the method
def get_c_diff(oldfileversion ,newfileversion):
    global oldfile 
    oldfile = oldfileversion
    global newfile
    newfile = newfileversion
    
    os.system("diff " + oldfile + " " + newfile + " | grep ^[0-9] > changes.txt")
    fo = open("changes.txt", "r+")
    for line in fo.readlines():
        oldRange = re.compile("a|d|c").split(line)[0]
        newRange = re.compile("a|d|c").split(line)[1]
        if oldRange.find(",") != -1:
            start = oldRange.split(",")[0]
            end = oldRange.split(",")[1]
            loc = start.strip(),end.strip()
            oldChanges.append(loc)
        
        else:
            loc = oldRange.strip(), oldRange.strip()
            oldChanges.append(loc)
        
        if newRange.find(",") != -1:
            start = newRange.split(",")[0]
            end = newRange.split(",")[1]
            loc = start.strip(),end.strip()
            newChanges.append(loc)
        
        else:
            loc = newRange.strip(), newRange.strip()
            newChanges.append(loc)
    
    fo.close()
    #print(oldChanges)
    #print(newChanges)
    get_method_range(oldfile)
    get_method_range(newfile)
    
    os.system("rm changes.txt")
    return get_changed_method()

#print(get_c_diff("../Tests/DistCalcTest/prev/main.c","../Tests/DistCalcTest/cur/main.c"))
