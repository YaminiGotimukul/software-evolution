import os
from change_methods import get_c_diff
from os import path

def traverse(prevPath, headPath):
    oldFiles = []
    newFiles = []
    methodlist = []
    os.system("find " + prevPath + " -name '*.c' > oldFiles.txt")
    os.system("find " + headPath + " -name '*.c' > newFiles.txt")
    foOld = open("oldFiles.txt", "r+")
    foNew = open("newFiles.txt", "r+")
    for line in foOld.readlines():
        oldFiles.append(line.strip())
    for line in foNew.readlines():
        newFiles.append(line.strip())
    for oldFile in oldFiles:    
        oldName = os.path.basename(oldFile).strip()
	if(oldName != "mtwist.c") :
            for newFile in newFiles :
                newName = os.path.basename(newFile).strip()
                if oldName == newName :
                    for method in get_c_diff(oldFile, newFile) :
                        methodlist.append(method)
    os.system("rm oldFiles.txt")
    os.system("rm newFiles.txt")
    methodSet = set(methodlist)
    return [func for func in methodSet]

if __name__ == "__main__":
    oldpath = "../Tests/DistCalcTest/versions/v1"
    newpath = "../Tests/DistCalcTest/versions/v2"
    print(traverse(oldpath, newpath))
