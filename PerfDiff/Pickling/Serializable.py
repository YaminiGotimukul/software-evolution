#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import pickle

class Node:

    def __init__ (self, function_name , line_number ):
        self.function_name = function_name
        self.line_number = line_number
        self.parent_list = []

    def get_parent_list(self):
        self.parent_list.append(Node("foo", 12))
        self.parent_list.append(Node("bar", 23))
        self.parent_list.append(Node("Xhi",40))
        self.parent_list.append(Node("getNumber",15))
        print "Displaying parents"
        for list in self.parent_list :
            print list.display_node()
        
        return self.parent_list 

    def add_parent_list(self, node):
        self.get_parent_list().append(node)
        print "Adding to the list"

    def display_node(self):
        print "Function name " + self.function_name + " line number " + str(self.line_number)

    def save_instance_state(self,node):
         pickle.dump(node, file('node.pickle','w'))

    def load_instance(self):
        un_pickled = pickle.load(file('node.pickle'))
        print "Reading unpickled Data ....."
        un_pickled.display_node()
        un_pickled.get_parent_list()

node = Node("Chi",123)
node2 = Node("GHI" ,1232)
node.save_instance_state(node)
node.load_instance()