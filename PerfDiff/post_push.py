#!/usr/bin/python

import os
import sys
import pickle
import subprocess
import ConfigParser

def post_comment(repo, version, comment, user, access_token):
	os.system('curl --user "' + user + ':' + access_token + '" --data ' + comment + ' https://api.github.com/repos' + repo + '/commits/' + version + '/comments'); 

def create_comment(path, line, message)
	return '{ "body": "' + message + '", "path": "' + path + '", "position":' + line + ', "line": null	}'

config = ConfigParser.RawConfigParser()
config.read('/Config/PerfDiff.cfg')
	
github = config.get('PerfDiff','GITHUB_REPOSITORY')
access = config.get('PerfDiff','GITHUB_ACCESS_TOKEN')
user = config.get('PerfDiff','GITHUB_USERNAME')

fileObject = open("perf_diff_post",'r')  
obj = pickle.load(fileObject) 

for x in range(0, obj.size):
	comment = create_comment(obj.node[x].path, obj.node[x].line, obj.node[x].message)
	post_comment(github, obj.revision, comment, user, access)

sys.exit(0)
