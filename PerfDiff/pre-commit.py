#!/usr/bin/python

'''
Inputs: version
		src directory (defaults to working directory)
					
'''

'''
Inputs (for now) :	previous version directory
					current version directory
					file with interested functions
'''
import sys
import ConfigParser
# import pandas
from profiler import runProfiler, generateAnnotations
from compileTest import create_bin
from read_test_input import store_interested_test
from traversal import traverse
from prettytable import PrettyTable

# read config file
config = ConfigParser.RawConfigParser()
config.read('/Config/PerfDiff.cfg')

github = "https://github.com" + config.get('PerfDiff','GITHUB_REPOSITORY')
access = config.get('PerfDiff','GITHUB_ACCESS_TOKEN')
rev = config.get('PerfDiff','REVISON_HASH')
curr = config.get('PerfDiff','WORKING_DIR')
tmp =  config.get('PerfDiff','TEMP_DIR')

# generate the binaries
create_bin(github, rev, tmp, curr)

# get the changed methods
requestedFuncs = ""
changedFuncs = [func[0] for func in traverse(tmp, curr)]
# changedFuncs = ["func1", "func0", "func2"]
print("Diff staring...")
print changedFuncs
print("these are changed functions.")
# run the profiler on each binary
prev = runProfiler(tmp)
head = runProfiler(curr)

listForSerialize = generateAnnotations(prev, head, changedFuncs, [("ScanDistMap",("dummy.c",30))])

# allFuncsdict = dict(allFuncs)

# get tuples of changed funcs and their speeds 
# changedFuncsSpeed =[(func, allFuncsdict[func]) for func in set(allFuncsdict).intersection((set(changedFuncs)))]
'''
# make the results pretty
chgTable = PrettyTable(["Changed Functions", "SpeedUp Factor"])
chgTable.align["Changed Functions"] = "l" # Left align city names
chgTable.padding_width = 1 # One space between column edges and contents (default)
for (func,speed) in changedFuncsSpeed:
	chgTable.add_row([func,speed])

print chgTable

# get tuples of requested funcs and their speeds 
reqFuncsSpeed =[(func, allFuncsdict[func]) for func in set(allFuncsdict).intersection((set(requestedFuncs)))]

# make the results pretty
reqTable = PrettyTable(["Requested Functions", "SpeedUp Factor"])
reqTable.align["Requested Functions"] = "l" # Left align city names
reqTable.padding_width = 1 # One space between column edges and contents (default)
for (func,speed) in reqFuncsSpeed:
	reqTable.add_row([func,speed])

print reqTable
'''
sys.exit(0)
