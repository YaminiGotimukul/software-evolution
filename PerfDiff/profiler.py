

'''
Inputs: Binary to run profiler on
		Functions requested

'''

import subprocess
import itertools
from traversal import traverse

def runProfiler( binFilePath ):

	# sampleRate = 100

	# gprof parsing

	# run the binary
	#binProc = subprocess.Popen("./"+binFilePath, shell=True)
	#binProc.communicate()

	# run the profiler on the binary 
	#profProc = subprocess.Popen("gprof "+binFilePath+" gmon.out", stdout=subprocess.PIPE, shell=True)

	# goes from 'Flat' to " %"
	#flatStrip = list(itertools.takewhile(lambda x: x != '\n', itertools.dropwhile(lambda x: 'time   s' not in x, profProc.stdout)))
	# pop off the top line
	#flatStrip = flatStrip[1:]



	# run the binary

	binProc = subprocess.Popen(binFilePath+"/prog", stdout=subprocess.PIPE, shell=True)

	#dump = binProc.communicate()

	#print dump
	print "Running profiler"
	# run the profiler on the binary 
	profProc = subprocess.Popen("gprof "+binFilePath+"/prog "+binFilePath+"/gmon.out", stdout=subprocess.PIPE, shell=True)

	# goes from 'Flat' to " %"
	flatStrip = list(itertools.takewhile(lambda x: x != '\n', itertools.dropwhile(lambda x: 'time' not in x, profProc.stdout)))
	# pop off the top line
	#flatStrip = flatStrip[1:]

	if not flatStrip:
		return
	#totalSamples = int(flatStrip[0].split()[1])
	#baseExec = 0.0

	flatStrip = flatStrip[1:]
	if not flatStrip:
		return

	baseLine = [line for line in flatStrip if "base" in line]
	if not baseLine:
		return
	print baseLine[0]
	baseExec = float(baseLine[0].split()[5])

	execList = []
	'''
	# traverse all the tokens
	for line in flatStrip:
		#if (any(subString in line for subString in funcRequestedList+funcChangedList)):
		for func in (funcRequestedList+funcChangedList):
			if (func in line):
				curExec = float(line.split()[5])
				func = string(line.split()[6])
				weightedExec = curExec/baseExec
				execList.append((func,weightedExec))
				print line,
	'''
	# traverse all the tokens
	for line in flatStrip:
		#if (any(subString in line for subString in funcRequestedList+funcChangedList)):
		print line,
		tokens = line.split()
		if (len(tokens) < 6):
			continue
		curExec = float(tokens[5])
		func = tokens[6]
		weightedExec = curExec/baseExec
		execList.append((func,weightedExec))

	if profProc.stdout:
		profProc.stdout.close()
	
	print execList
	return execList
	#print profDump

def generateAnnotations ( execPrev, execHead, changedFuncs, funcLines ):
	if ((not execHead) or (not execPrev)):
		return

	prevDict = dict(execPrev)
	headDict = dict(execHead)
	allFuncs = []
	for func in set(headDict).intersection(set(prevDict)):
		if float(prevDict[func]) == 0.0 or float(headDict[func]) == 0.0 :
			speedFactor = 1.0
		else:
			speedFactor = float(prevDict[func]/headDict[func])
		allFuncs.append((func,speedFactor))

	allFuncsDict = dict(allFuncs)

	changedFuncsSpeed =[(func, allFuncsDict[func]) for func in set(allFuncsDict).intersection((set(changedFuncs)))]

	funcLinesDict = dict(funcLines)

	funcsToAnnotate = []
	for func in set(allFuncsDict):
		if ((allFuncsDict[func] > 1.1) or (allFuncsDict[func] < 0.9)):
			funcsToAnnotate.append((func, allFuncsDict[func], funcLinesDict[func][0], funcLinesDict[func][1], changedFuncsSpeed))
			#funcsToAnnotate.append(func, funcLinesDict[func], allFuncsDict[func], changedFuncsSpeed)
	print "Generating Annotations"
	print funcsToAnnotate
	return funcsToAnnotate
	#return [(func, prevDict[func]/headDict[func]) for func in (set(prevDict).intersection(set(headDict)))]

prev = runProfiler("~/software-evolution/Tests/DistCalcTest/versions/v1")
head = runProfiler("~/software-evolution/Tests/DistCalcTest/versions/v2")

#changedFuncs = [func[0] for func in traverse("~/software-evolution/Tests/DistCalcTest/versions/v1", "~/software-evolution/Tests/DistCalcTest/versions/v2")]
changedFuncs = ["ScanDistMap"]
generateAnnotations(prev, head, changedFuncs, [(ScanDistMap,("dummy.c",30))])
#head = runProfiler("prof",["func1", "func0", "func2"], ["pow", "main"])

#print computeSpeedUp(prev, head)




