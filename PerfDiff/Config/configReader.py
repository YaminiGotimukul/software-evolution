#!/urs/bin/python
# -*- coding: utf-8 -*-

import ConfigParser

def read_config_file():
    config = ConfigParser.RawConfigParser()
    config.read('perfDiff-config.cfg')
    return config

def get_url():
    config = read_config_file()
    print "URL Retrieved : "+ config.get('Initial_Set_Up','url')
    return config.get('Initial_Set_Up','url')

def get_current_version_path():
    config = read_config_file()
    print "Current version path : "+ config.get('Initial_Set_Up','current version path')
    return config.get('Initial_Set_Up','current version path')

def get_previou_version_path():
    config = read_config_file()
    print "Previous version path : "+ config.get('Initial_Set_Up','old version path')
    return config.get('Initial_Set_Up','old version path')

def get_thershold():
    config = read_config_file()
    print "Thershold set up by user : "+ config.get('Initial_Set_Up','thershold')
    return config.get('Initial_Set_Up','thershold')

def get_user_name():
    config = read_config_file()
    print "User name : "+ config.get('Initial_Set_Up','user name')
    return config.get('Initial_Set_Up','user name')

def get_token_id():
    config = read_config_file()
    print "Token Id : "+ config.get('Initial_Set_Up','token id')


get_url()
get_current_version_path()
get_previou_version_path()
get_thershold()
get_user_name()
get_token_id()
