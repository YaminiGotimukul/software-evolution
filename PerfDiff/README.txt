1. install pycparser:
   a. go to the directory of pycparser
   b. execute "python setup.py install".
note: pycparser was tested on Python 2.6, 2.7 and 3.2, on both Linux and Windows. It should work on any later version (in both the 2.x and 3.x lines) as well.

2. API:
   a. get_c_diff in change_methods,py : 
       input  - oldfileversion : the path for c file in old revision
              - newfileversion : the path for c file in new revision
       output - a list of changed methods. Each method is represented by a tuple, (function name, function start position).   
   b. traverse in traversal.py :
       input  - prevPath : the path for previous version
                headPath : the path for head version
       output - a list of changed methods in all files between old and new version. Each method is represented by a tuple, (function name, function start position).   
