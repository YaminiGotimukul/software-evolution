#!/usr/bin/python

from git import Repo
from git import Git
import os
import sys
import subprocess
import shutil

def is_exe(fpath):
	return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

def create_bin(git_url, version, repo_dir, src_dir):
	shutil.rmtree(repo_dir)
	os.mkdir(repo_dir)
	Repo.clone_from(git_url, repo_dir)
	g = Git(repo_dir)
	g.checkout(version)

	result = subprocess.Popen(['make', 'clean'], stdout=subprocess.PIPE, cwd=src_dir)
	result.wait();
	result = subprocess.Popen(["make"], stdout=subprocess.PIPE, cwd=src_dir)
	result.wait();
	result = subprocess.Popen(['make', 'clean'], stdout=subprocess.PIPE, cwd=repo_dir)	
	result.wait();
	result = subprocess.Popen(["make"], stdout=subprocess.PIPE, cwd=repo_dir)
	result.wait();
	
	exe_lst = []
	code_dir = [src_dir, repo_dir]
	
	for code_path in code_dir:
		for root, dirs, files in os.walk(code_path):
			for file in files:
				exe_file = os.path.join(code_path, file)
				if is_exe(exe_file):
					print(exe_file)
					exe_lst.append(exe_file)
	
	return exe_lst
	
# create_bin("https://github.com/noah-/test.git", "60c3db8e4862e4b7d1e83a5ae2dba78b611480c5", "../Tests/DistCalcTest/prev",  "../Tests/DistCalcTest/cur")
# call gprof script 
